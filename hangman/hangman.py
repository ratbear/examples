#!/usr/bin/env python3
import random
import urllib.request


class Hangman(object):
    def __init__(self):
        self.words = ['horse', 'house', 'tree', 'duck', 'oracle']
        self.word_site = "\
        https://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
        self.guess_status = None
        self.word = None
        self.guess_num = 0
        self.new_word()
        self.man = [
            '______',
            '|   |',
            '|   O',
            '|   |',
            '|  /|\\',
            '|   | ',
            '|  / \\',
            '|'
        ]

    def new_word(self, cached=False):
        # TODO cache word list to a file for offline use
        if not cached:
            response = urllib.request.urlopen(self.word_site)
            site_words = response.read()
            words = site_words.splitlines()
            choice = str(random.choice(words), 'utf-8')
        else:
            choice = random.choice(self.words)

        self.word = list(choice.lower())
        self.guess_status = ['_' for x in self.word]

    def guess_prompt(self):
        print("#" * 79)
        return input("\nPlease enter a letter: ").lower()

    def make_guess(self, guess):
        """ Update the guess status if the given guess is found in a the word.
        :param guess: (str) value to check for in the word.
        """
        if guess in self.word:
            changes = 0
            for position, letter in enumerate(self.word):
                if letter == guess.lower():
                    changes += 1
                    self.guess_status[position] = guess
            print("\nGood job! Found {0} {1}'s!".format(changes, guess))
            return True
        else:
            print("\nSorry, incorrect guess: {0}".format(guess))
            self.guess_num += 1
            return False

    def draw_man(self):
        man = self.man[:self.guess_num]
        print('\n'.join(man))


def banner(words):
    blockline = "#" * 79
    print("\n{0}\n{1}\n{2}\n".format(blockline, words, blockline))


def play():
    print('\nWelcome to Hangman!\n')
    game = Hangman()
    print(game.word)
    print(game.guess_status)
    while game.guess_num < len(game.man):
        game.draw_man()
        # guess = input(guess_prompt()).lower()
        guess = game.guess_prompt()
        game.make_guess(guess)
        game.draw_man()
        print(game.guess_status)
        if game.guess_status == game.word:
            banner('You Win!')
            return
        elif game.guess_num == len(game.man):
            banner('You Lose!')
            return
        else:
            print('Try Again ... ')


if __name__ == '__main__':
    play()
