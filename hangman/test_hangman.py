#!/usr/bin/env python3

from hangman import Hangman
import pytest


def test_create_hangman_word():
    """Verify a word is selected from the word list."""
    gallows = Hangman()
    assert isinstance(gallows.word, list)
    assert ''.join(gallows.word) in gallows.words


def test_create_hangman_guessStatus():
    """Verify the guess status has a 'blank' for each letter in word."""
    gallows = Hangman()
    assert len(gallows.guess_status) == len(gallows.word)
    for i in gallows.guess_status:
        assert i == '_'


def test_new_word_normalized():
    """Verify if word is set with upcase values the are converted to lowercase.
    """


def test_makeguess_caseInsensitive():
    """Verify an upper case guess gets nomalized to lower case."""
    gallows = Hangman()
    gallows.new_word('someselectedword')
    gallows.make_guess('O')
    assert gallows.guess_status == ['_', 'o', '_', '_', '_', '_', '_', '_',
                                    '_', '_', '_', '_', '_', 'o', '_', '_']


def test_makeguess_multiple_matches(capsys):
    """"Verify if a word contains more that one of the letter guessed,
    both are updated."""
    test = Hangman()
    test.new_word('testing')
    if not test.make_guess('t'):
        assert False
    out, err = capsys.readouterr()
    assert out == "\nGood job! Found 2 t's!\n"
    assert test.guess_status == ['t', '_', '_', 't', '_', '_', '_']


def test_guesscorrect_man():
    """Verify a correct guess not cause parts to be added to the man"""


def test_makeguess_notFound(capsys):
    """Verify if the guess is not in the word, the guess status does not change
    and a message is displayed."""
    test = Hangman()
    test.new_word('abc123')
    test.make_guess('d')
    out, err = capsys.readouterr()
    assert out == "\nSorry, incorrect guess: d\n"
    assert test.guess_status == ['_', '_', '_', '_', '_', '_']


def test_guessNotFound_man():
    """Verify an incorrect guess adds a part to the man."""


def teardown_function():
    """
    """
