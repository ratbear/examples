#!/usr/bin/env python

__version__ = 0.1
__author__ = "Steve Ybarra"

import time
import getpass
import yaml
import argparse
from pprint import pprint
from novaclient import client
from novaclient.exceptions import NotFound

def ParseArgs():
    parser = argparse.ArgumentParser(description="Adding some sut servers")
    parser.add_argument('-f', '--infile', help="Input file containing server information")
    
    global args
    args = parser.parse_args()

def ReadYaml(fname):
    """Read the given file and return a python list
    :param fname: (str) the path to the input file.
    :return: (list) server info
    """
    with open(fname, 'r') as srvinfo:
        return yaml.load(srvinfo)
    
def ServerExists(nvclient, name):
    """Check if server instance already exists using the instance name.
    :param nvclient: (object)
    :param name: (str)
    """
    try:
        nvclient.servers.find(name=name)
        return True
    except NotFound as nf:
        return False

def NetNametoId(nvclient, networks):
    """Give a Convert the human readable network names to network ids.
    :param networks: (list(dict)) [{net-id: <human_id1>}, {net-id: <human_id2>}] 
    (Ex. [{net-id: "xmi"}, {net-id: "imi"])
    :return: (list(dict)) return the same dictionary list that was passed in after
    updating the net-id value to the appropriate uuid. 
    (Ex. [{net-id: u'3ad7674b-5026-4721-9a3a-9f81c99f465a'}]) 
    """
    for net in networks:
        netobj = nvclient.networks.find(human_id=net['net-id'])
        net['net-id'] = netobj.__getattr__('id')
    return networks 


def CreateInstance(nvclient, name, image, flavor, nics):
    """Start instance and monitors status until build is complete.
    :param nvclient: (object) The novaclient client object 
    :param name: (str) desired name of the instance being created
    :param image: (str) name of the image to use when creating the instance
    :param flavor: (str) name of the flavor to use when creating the instance
    :param nics: (list) a list of dictionaries containing nic data.
    (Ex. [{"net-id": "phrogger-xmi", "v4-fixed-ip": "100.100.100.13"},
          {"net-id": "phrogger-imi", "v4-fixed-ip": "200.200.200.13"},])
    """
    img = nvclient.images.find(name=image)
    flv = nvclient.flavors.find(name=flavor)
    nics = NetNametoId(nvclient, nics)
    instance = nvclient.servers.create(name=name, image=img, flavor=flv, nics=nics)

    #Verify Build completes
    status = instance.status
    while status == 'BUILD':
        time.sleep(5)
        instance = nvclient.servers.get(instance.id)
        status = instance.status
    print("[+] {} Status: {}".format(name, status))
        


def main():
    ParseArgs()
    ###########################################################################
    # OpenStack Credentials
    ###########################################################################
    version = '2'
    username = 'leonidas'
    password = getpass.getpass("OpenStack Password: ")
    project = 'MyProject'
    auth_url = 'http://<ip_address>:5000/v2.0'

    with client.Client(version, username, password, project, auth_url) as nova:
        srvinfo = ReadYaml(args.infile)
        for srv in srvinfo:
            #Skip if server alread exists
            if ServerExists(nova, srv.get('name')): 
                print ('[+] Server already exists, skipping : {}'.format(
                    srv.get('name')))
                continue
            #Create Server
            CreateInstance(
                nvclient=nova,
                name=srv.get('name'),
                image=srv.get('image'),
                flavor=srv.get('flavor'),
                nics=srv.get('nics'))
        print(nova.servers.list())

if __name__ == '__main__':
    main()
