#!/usr/bin/env python3
"""
Code snippets
"""

def unique(a):
    """ return the list with duplicate elements removed """
    return list(set(a))

def intersect(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))

def union(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))

def prompt(self, prompt):
    """ Return a user prompt """
	return input(prompt).strip()
		
if __name__=='__main__':
    print('This file contains my code snippets')


