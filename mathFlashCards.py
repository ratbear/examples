#!/usr/bin/env python3

import math
import random
import os


def build_addtion_problem(num_list):
    os.system('clear')
    addend1 = random.choice(num_list)
    addend2 = random.choice(num_list)
    my_answer = 0
    correct_answer = sum([addend1, addend2])
    while my_answer != correct_answer:
        my_answer = int(input('Solve this equation: {0} + {1} = '.format(
            addend1, addend2)))

        if my_answer == correct_answer:
            print('Good Job! You got the right answer! ')
            return True
        else:
            print('Sorry that is not the right answer...')
            again = input('Would you like to try again (y/n)? ')
            if again.lower() in ['y', 'yes']:
                continue
            else:
                print('The correct answer was {}'.format(correct_answer))
                return False


def set_level():
    levels = [
        ('Kindergarten', [x for x in range(11)]),
        ('First Grade', [x for x in range(100)])
    ]
    print('Select a level...')
    for i, level in enumerate(levels):
        print('{}. {}'.format(i, level[0]))

    selection = int(input('Enter the Menu number : '))
    return levels[selection][1]


def play():
    total_correct = 0
    total_missed = 0
    os.system('clear')
    print('Welcome to Math Flash Cards!')
    level = set_level()
    play_again = True
    while play_again:
        if build_addtion_problem(level):
            total_correct += 1
        else:
            total_missed += 1

        play_again = input('Want another equation (y/n)? ')
        if play_again.lower() in ['y', 'yes']:
            play_again = True
        else:
            play_again = False
            print('Thanks for playing!')
            if total_correct > total_missed:
                print('You did Great!')
            else:
                print('I hope you will come back to play with me again soon!')

            print('You worked on {} equations today!'.format(
                sum([total_correct, total_missed])))
            print('{} correct'.format(total_correct))
            print('{} wrong'.format(total_missed))


if __name__ == '__main__':
    play()
